function startUp() {}

var riceData = {
    labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
    datasets: [

        {
            fillColor: "rgba(240, 107, 95,0.5)",
            data: [0, 0, 0, 0, 0, 4, 5, 5, 3, 0]
        },
        {
            fillColor: "rgba(255, 224, 1,0.6)",
            data: [0, 2, 1, 2, 4, 1, 1, 1, 1, 0]
        },
        {
            fillColor: "rgba(26, 213, 222,0.9)",
            data: [0, 1, 3, 1, 2, 5, 3, 2, 3, 0]
        }



    ]
}

var rice = document.getElementById('salesGraph').getContext('2d');
new Chart(rice).Line(riceData);

$(document).ready(startUp);