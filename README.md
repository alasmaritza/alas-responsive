What do you think of our current website and how would you improve it?

The current website is easy to navigate and has a good color scheme. I would improve the site by minimizing the amount of text on the home page to make it a bit less busy.